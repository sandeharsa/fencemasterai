package aiproj.fencemaster;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;

import aiproj.fencemaster.base.*;

/**
 * /** COMP30024 Artificial Intelligence
 *  Player AI
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

public class Sharsa implements Player, Piece{
	
	private static final int MMSTART = 8;
	public int MMDEPTH = 8;
	Board board;
	int piece;
	char type;
	Move lastmove;
	
	public final static int WIN = 10;
	public final static int LOSE = -10;
	// value for the positions in the board
	public static final int V1 = 1;
	public static final int V2 = 2;
	public static final int V3 = 3;
	public static final int V4 = 4;
	public static final int V5 = 5;
	
	public int totalHeuristic = 0;
	
	public int TRESHOLD = 200;
	
	
	@Override
	public int init(int n, int p) {
		board = new Board(n);
		piece = p;
		if (piece == WHITE)
			type = 'W';
		else if (piece == BLACK)
			type = 'B';
		// TODO return -1 when it does not initialize successfully
		else
			return -1;
		return 0;
	}

	@Override
	public Move makeMove() {
		int row;
		int col;
		Random rand = new Random();
		Move move = new Move(piece, false, 0, 0);
		Node curnode = new Node(move, 0);
		MinimaxTree mmt = new MinimaxTree(curnode);
		
		// Top priority to check whether this player can win in the next state
		if (doWeWin(move))
		{
			if (doMove(move) == 0)
			{
				return move;
			}
		}
		
		// Priority to check whether the opponent's is one move away from winning
		if (goBlock(move))
		{
			if (doMove(move) == 0)
			{
				return move;
			}
		}
		
		// Use Minimax
		if (Board.emptyCells.size() < MMSTART) {
			if (this.piece == WHITE) {
				System.out.println("Start minimax");
				minimax('W', mmt.get_root(), MMDEPTH);
			}
			else {
				System.out.println("Start minimax");
				minimax('B', mmt.get_root(), MMDEPTH);
			}
			return mmt.get_bestmove();
		}
		
		// Randomize move from neighbor
		if (randomizeNeighbor(move))
		{
			if (doMove(move) == 0)
			{
				return move;
			}
		}
		
		// Randomize move
		while(true)
		{
			row = rand.nextInt(Board.getSize());
			if(row < Board.N)
			{
				col = rand.nextInt(Board.getUEP(row)+1);
			}
			else
			{
				col = Board.getLSP(row) + rand.nextInt(Board.getLEP()+1 - Board.getLSP(row));
			}
			
			move = new Move(piece, false, row, col);
			// Insert the piece to the board
			if(doMove(move) == 0)
			{
				// Return the move
				return move;
			}
		}
	}

	@Override
	public int opponentMove(Move m) {
		// Check whether it is legal or not
		if(Board.TotalCell-1 != Board.emptyCells.size() && m.IsSwap) {
			/* no swap when it is not the first move */
			return 0;
		}
		doMove(m);
		return 1;
	}

	
	@Override
	public int getWinner() {
		boolean whiteT, whiteL;
		boolean blackT, blackL;
		boolean finalstate;
		
		// Check which disk wins and by what method
		blackL = WinChecker.LoopChecker('B');
		whiteL = WinChecker.LoopChecker('W');
		blackT = WinChecker.TripodChecker('B');
		whiteT = WinChecker.TripodChecker('W');
		
		// check if the board is in final state
		// which is the board is full of disks, no empty space
		finalstate = true;
		search:
		for (int i=0; i<Board.getSize(); i++) {
			for (int j=0; j<Board.getSize(); j++) {
				if (Board.getCell(i, j) == null) {
					continue;
				} else if (Board.getCell(i, j).getStatus() == 0) {
					finalstate = false;
					break search;
				}
			}
		}
		
		// return result
		if(blackL || blackT)
			return BLACK;
		else if(whiteL || whiteT)
			return WHITE;
		else if(finalstate)
			return EMPTY;
		else
			return INVALID;
	}

	
	@Override
	public void printBoard(PrintStream output) {
		Board.printBoard();
	}
	
	public int doMove(Move move) {
		if(move.IsSwap)
		{
			undoMove(lastmove);
		}
		lastmove = move;
		if(Board.getCell(move.Row, move.Col) == null)
		{
			System.out.printf("%d, %d\n", move.Row, move.Col);
			System.out.println("Invalid move");
		}
		if(Board.getCell(move.Row, move.Col).getDisk() == null)
		{
			char type;
			if(move.P == WHITE)
				type = 'W';
			else
				type = 'B';
			Board.getCell(move.Row, move.Col).placeDisk(move.Row, move.Col, type);
			totalHeuristic += heuristic(move);

			return 0;	
		}
		else
		{
			return -1;
		}
	}

	/**
	 *  This function is used for checking if the next move this player can win
	 *  @param move The move will be made
	 * 	@return true if there is a win state, false if there is no win state
	 */
	public boolean doWeWin(Move move) {
		char type;
		Move tempMove;
		ArrayList<Disk> diskArray;
		int x, y;
		
		if (this.piece == WHITE)
		{
			type = 'W';
			diskArray = Board.diskWhite;
		}
		else
		{
			type = 'B';
			diskArray = Board.diskBlack;
		}
		
		if (Board.getAllDisk(type).size() == 0)
		{
			return false;
		}
		for (int i = 0; i < Board.getAllDisk(type).size(); i++) {
			x = diskArray.get(i).getX();
			y = diskArray.get(i).getY();
			for (int j = 0; j < Board.getCell(x, y).getAdjacentCells(x, y).size(); j++) {
				if (Board.getCell(x, y).getAdjacentCells(x, y).get(j).getStatus() == 0) {
					int xm = Board.getCell(x, y).getAdjacentCells(x, y).get(j).getX();
					int ym = Board.getCell(x, y).getAdjacentCells(x, y).get(j).getY();
					tempMove = new Move(this.piece, false, xm, ym);
					doMove(tempMove);
					if (WinChecker.LoopChecker(type) || WinChecker.TripodChecker(type)) {
						undoMove(tempMove);
						move = tempMove;
						return true;
					}
					undoMove(tempMove);
				}
			}
		}
		// No win state
		return false;
	}
	
	/**
	 *  This function is used for blocking opponent's movement from winning.
	 *  @param move The move will be made
	 * 	@return true if need to block, false if no need to block
	 */
	public boolean goBlock(Move move) {
		char oppType;
		Move tempMove;
		ArrayList<Disk> oppDiskArray;
		int x, y;
		
		if (this.piece == WHITE)
		{
			oppType = 'B';
			oppDiskArray = Board.diskBlack;
		}
		else
		{
			oppType = 'W';
			oppDiskArray = Board.diskWhite;
		}
		
		if (Board.getAllDisk(oppType).size() == 0)
		{
			return false;
		}
		for (int i = 0; i < Board.getAllDisk(oppType).size(); i++) {
			x = oppDiskArray.get(i).getX();
			y = oppDiskArray.get(i).getY();
			for (int j = 0; j < Board.getCell(x, y).getAdjacentCells(x, y).size(); j++) {
				if (Board.getCell(x, y).getAdjacentCells(x, y).get(j).getStatus() == 0) {
					int xm = Board.getCell(x, y).getAdjacentCells(x, y).get(j).getX();
					int ym = Board.getCell(x, y).getAdjacentCells(x, y).get(j).getY();
					tempMove = new Move(this.piece, false, xm, ym);
					doMove(tempMove);
					if (WinChecker.LoopChecker(oppType) || WinChecker.TripodChecker(oppType)) {
						undoMove(tempMove);
						move = tempMove;
						return true;
					}
					undoMove(tempMove);
				}
			}
		}
		// No need to block
		return false;
	}
	
	/**
	 * Make a random move near the player's disks.
	 * @param move The move will be made
	 * @return true if there is an empty cell in adjacent cells, false if there is none
	 */
	public boolean randomizeNeighbor(Move move) {
		int maxval = 0;
		char type;
		ArrayList<Disk> diskArray;
		int x, y;
		
		if (this.piece == WHITE)
		{
			type = 'W';
			diskArray = Board.diskWhite;
		}
		else
		{
			type = 'B';
			diskArray = Board.diskBlack;
		}

		if (Board.getAllDisk(type).size() == 0)
		{
			return false;
		}
		for (int i = 0; i < Board.getAllDisk(type).size(); i++) {
			x = diskArray.get(i).getX();
			y = diskArray.get(i).getY();
			for (int j = 0; j < Board.getCell(x, y).getAdjacentCells(x, y).size(); j++) {
				if (Board.getCell(x, y).getAdjacentCells(x, y).get(j).getStatus() == 0) {
					int xm = Board.getCell(x, y).getAdjacentCells(x, y).get(j).getX();
					int ym = Board.getCell(x, y).getAdjacentCells(x, y).get(j).getY();
					if(maxval < heuristic(move)) {
						move.Col = ym;
						move.Row = xm;
						maxval = heuristic(move);
					}
				}
			}
		}
		if(maxval == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * This function is used to undo a move
	 * @param m The move
	 */
	public void undoMove(Move move) {
		int x = move.Row;
		int y = move.Col;
		char type = (move.P == 1) ? 'W' : 'B';
		Cell cell = Board.getCell(x, y);
		ArrayList<Cell> adjacentCells = cell.getAdjacentCells(x, y);
		
		for(int i = 0; i < adjacentCells.size(); i++) {
			if (adjacentCells.get(i).getStatus() == 1) {
				if (adjacentCells.get(i).getDisk().getType() == cell.getDisk().getType()) {
					adjacentCells.get(i).getDisk().getNeighbor().remove(cell.getDisk());
				}
			}
		}
		
		if(type == 'W') {
			Board.diskWhite.remove(cell.getDisk());
			Board.getCell(x, y).disk = null;
			Board.getCell(x, y).status = 0;
		}
		else {
			Board.diskBlack.remove(cell.getDisk());
			Board.getCell(x, y).disk = null;
			Board.getCell(x, y).status = 0;
		}
		Board.emptyCells.add(cell);
		totalHeuristic -= heuristic(move);
	}
	
	/**
	 * This is a minimax function to choose the best move.
	 * This is a recursive function.
	 * @param type the type of disk ('W' / 'B')
	 * @param curnode current node from the tree
	 */
	public void minimax(char type, Node curnode, int depth) {
		int x, y;
		int cur_piece;
		char oppType;
		int curvalue = 0, maxvalue = -TRESHOLD, minvalue = TRESHOLD;
		Move move = new Move(piece, false, 0, 0);
		
		if (depth < 0) {
			return;
		}
		
		if (type == 'W') {
			cur_piece = WHITE;
			oppType = 'B';
		}
		else {
			cur_piece = BLACK;
			oppType = 'W';
		}
		int limit = Board.emptyCells.size();
		for (int i = 0; i < limit; i++) {
			
			Node n = new Node(move, curvalue);
			x = Board.emptyCells.get(i).getX();
			y = Board.emptyCells.get(i).getY();
			if (Board.getCell(x, y) != null) {
				move.Col = y;
				move.Row = x;
				if (Board.getCell(x, y).getStatus() == 0) {
					doMove(move);
				}
				// if one of the players wins
				if (WinChecker.LoopChecker(type) || WinChecker.TripodChecker(type)) {
					if (this.piece == cur_piece) {
						totalHeuristic += WIN;
					}
					else {
						totalHeuristic += LOSE;
					}
				}

				if(Board.getCell(x, y).getDisk().getNeighbor().size() > 0) {
					minimax(oppType, n, depth-1);
				}
				if (depth <= 1) {
					n.update_value(totalHeuristic);
				}
				if (n.get_value() > maxvalue) { maxvalue = n.get_value(); }
				if (n.get_value() < minvalue) { minvalue = n.get_value(); }
				n.add_parent(curnode);
				undoMove(move);
			}
		}
		if (this.piece == cur_piece) { curnode.update_value(maxvalue); }
		else { curnode.update_value(minvalue); }
	}

	public int heuristic(Move move) {
		int hvalue, a;
		
		if (this.piece == move.P) a = 1;
		else a = -1;
		
		if ((move.Row == 1 && move.Col == Board.getUSP()+1) ||
				(move.Row == 1 && move.Col == Board.getUEP(1)-1) ||
				(move.Row == Board.N-1 && move.Col == Board.getUSP()+1) ||
				(move.Row == Board.N-1 && move.Col == Board.getUEP(Board.N-1)-1) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLSP(Board.getSize()-2)+1) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLEP()-1)) {
			hvalue = a*V5;
		}
		else if ((move.Row == 0 && move.Col == Board.getUSP()+1) ||
				(move.Row == 1 && move.Col == Board.getUSP()) ||
				(move.Row == 1 && move.Col == Board.getUSP()+2) ||
				(move.Row == 2 && move.Col == Board.getUSP()+1) ||
				(move.Row == 0 && move.Col == Board.getUEP(0)-1) ||
				(move.Row == 1 && move.Col == Board.getUEP(1)-2) ||
				(move.Row == 1 && move.Col == Board.getUEP(1)) ||
				(move.Row == 2 && move.Col == Board.getUEP(2)-1) ||
				(move.Row == Board.N-2 && move.Col == Board.getUSP()) ||
				(move.Row == Board.N-2 && move.Col == Board.getUSP()+1) ||
				(move.Row == Board.N && move.Col == Board.getLSP(Board.N)) ||
				(move.Row == Board.N && move.Col == Board.getLSP(Board.N)+1) ||
				(move.Row == Board.N-2 && move.Col == Board.getUEP(Board.N-2)) ||
				(move.Row == Board.N-2 && move.Col == Board.getUEP(Board.N-2)-1) ||
				(move.Row == Board.N && move.Col == Board.getLEP()) ||
				(move.Row == Board.N && move.Col == Board.getLEP()-1) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLSP(Board.getSize()-3)+1) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLSP(Board.getSize()-2)) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLSP(Board.getSize()-2)+2) ||
				(move.Row == Board.getSize()-1 && move.Col == Board.getLSP(Board.getSize()-1)+1) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLEP()-1) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLEP()-2) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLEP()) ||
				(move.Row == Board.getSize()-1 && move.Col == Board.getLEP()-1)) {
			hvalue = a*V4;
		}
		else if ((move.Row == 0 && move.Col == 0) ||
				(move.Row == 1 && move.Col == 3) ||
				(move.Row == 2 && move.Col == 2) ||
				(move.Row == 3 && move.Col == 1) ||
				(move.Row == 0 && move.Col == Board.getUEP(0)) ||
				(move.Row == 1 && move.Col == Board.getUEP(0)-3) ||
				(move.Row == 2 && move.Col == Board.getUEP(0)-2) ||
				(move.Row == 3 && move.Col == Board.getUEP(0)-1) ||
				(move.Row == Board.N-3 && move.Col == Board.getUSP()+1) ||
				(move.Row == Board.N-1 && move.Col == Board.getUSP()) ||
				(move.Row == Board.N-1 && move.Col == Board.getUSP()+2) ||
				(move.Row == Board.N+1 && move.Col == Board.getLSP(Board.N+1)+1) ||
				(move.Row == Board.N-3 && move.Col == Board.getUEP(Board.N-3)-1) ||
				(move.Row == Board.N-1 && move.Col == Board.getUEP(Board.N-1)-2) ||
				(move.Row == Board.N-1 && move.Col == Board.getUEP(Board.N-1)) ||
				(move.Row == Board.N+1 && move.Col == Board.getUEP(Board.N+1)-1) ||
				(move.Row == Board.getSize()-4 && move.Col == Board.getLSP(Board.getSize()-4)+1) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLSP(Board.getSize()-3)+2) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLSP(Board.getSize()-2)+3) ||
				(move.Row == Board.getSize()-1 && move.Col == Board.getLSP(Board.getSize()-1)) ||
				(move.Row == Board.getSize()-4 && move.Col == Board.getLEP()-1) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLEP()-2) ||
				(move.Row == Board.getSize()-2 && move.Col == Board.getLEP()-3) ||
				(move.Row == Board.getSize()-1 && move.Col == Board.getLEP())) {
			hvalue = a*V3;
		}
		else if ((move.Row == 0 && move.Col == Board.getUSP()+2) ||
				(move.Row == 2 && move.Col == Board.getUSP()) ||
				(move.Row == 2 && move.Col == Board.getUSP()+3) ||
				(move.Row == 3 && move.Col == Board.getUSP()+2) ||
				(move.Row == 0 && move.Col == Board.getUEP(0)-2) ||
				(move.Row == 2 && move.Col == Board.getUEP(1)) ||
				(move.Row == 2 && move.Col == Board.getUEP(1)-3) ||
				(move.Row == 3 && move.Col == Board.getUEP(2)-2) ||
				(move.Row == Board.N-3 && move.Col == Board.getUSP()) ||
				(move.Row == Board.N-2 && move.Col == Board.getUSP()+2) ||
				(move.Row == Board.N && move.Col == Board.getLSP(Board.N)+2) ||
				(move.Row == Board.N+1 && move.Col == Board.getLSP(Board.N)) ||
				(move.Row == Board.N-3 && move.Col == Board.getUEP(Board.N-2)) ||
				(move.Row == Board.N-2 && move.Col == Board.getUEP(Board.N-2)-2) ||
				(move.Row == Board.N && move.Col == Board.getLEP()-2) ||
				(move.Row == Board.N+1 && move.Col == Board.getLEP()) ||
				(move.Row == Board.getSize()-4 && move.Col == Board.getLSP(Board.getSize()-4)+2) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLSP(Board.getSize()-3)) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLSP(Board.getSize()-3)+4) ||
				(move.Row == Board.getSize()-1 && move.Col == Board.getLSP(Board.getSize()-1)+2) ||
				(move.Row == Board.getSize()-4 && move.Col == Board.getLEP()-2) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLEP()) ||
				(move.Row == Board.getSize()-3 && move.Col == Board.getLEP()-4) ||
				(move.Row == Board.getSize()-1 && move.Col == Board.getLEP()-2)) {
			hvalue = a*V2;
		}
		else {
			hvalue = a*V1;
		}
		
		return hvalue;
	}
}
