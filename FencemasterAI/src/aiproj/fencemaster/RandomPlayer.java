package aiproj.fencemaster;
import java.io.PrintStream;
import java.util.Random;

import aiproj.fencemaster.base.*;

/**
 * /** COMP30024 Artificial Intelligence
 *  Player AI
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

public class RandomPlayer implements Player, Piece{
	
	Board board;
	int piece;
	
	@Override
	public int init(int n, int p) {
		board = new Board(n);
		piece = p;
		
		// TODO return -1 when it does not initialize successfully
		
		return 0;
	}

	@Override
	public Move makeMove() {
		int row;
		int col;
		Random rand = new Random();
		
		// Randomize move
		while(true)
		{
			row = rand.nextInt(Board.getSize());
			if(row < Board.N)
			{
				col = rand.nextInt(Board.getUEP(row)+1);
			}
			else
			{
				col = Board.getLSP(row) + rand.nextInt(Board.getLEP()+1 - Board.getLSP(row));
			}
			
			// Create the Move object
			Move move = new Move(piece, false, row, col);
			// Insert the piece to the board
			if(doMove(move) == 0)
			{
				// Return the move
				return move;
			}
		}
	}

	@Override
	public int opponentMove(Move m) {
		doMove(m);
		// Check whether it is legal or not
		return 0;
	}

	
	@Override
	public int getWinner() {
		boolean whiteT, whiteL;
		boolean blackT, blackL;
		boolean finalstate;
		
		// Check which disk wins and by what method
		blackL = WinChecker.LoopChecker('B');
		whiteL = WinChecker.LoopChecker('W');
		blackT = WinChecker.TripodChecker('B');
		whiteT = WinChecker.TripodChecker('W');
		
		// check if the board is in final state
		// which is the board is full of disks, no empty space
		finalstate = true;
		search:
		for (int i=0; i<Board.getSize(); i++) {
			for (int j=0; j<Board.getSize(); j++) {
				if (Board.getCell(i, j) == null) {
					continue;
				} else if (Board.getCell(i, j).getStatus() == 0) {
					finalstate = false;
					break search;
				}
			}
		}
		
		// return result
		if(blackL || blackT)
			return BLACK;
		else if(whiteL || whiteT)
			return WHITE;
		else if(finalstate)
			return EMPTY;
		else
			return INVALID;
	}

	
	@Override
	public void printBoard(PrintStream output) {
		Board.printBoard();
	}
	
	public int doMove(Move move) {
		if(Board.getCell(move.Row, move.Col) == null)
		{
			System.out.printf("%d, %d\n", move.Row, move.Col);
			System.out.println("Invalid move");
		}
		if(Board.getCell(move.Row, move.Col).getDisk() == null)
		{
			char type;
			if(move.P == WHITE)
				type = 'W';
			else
				type = 'B';
			Board.getCell(move.Row, move.Col).placeDisk(move.Row, move.Col, type);
			
			return 0;	
		}
		else
		{
			return -1;
		}
	}
}
