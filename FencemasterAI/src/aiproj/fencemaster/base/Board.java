package aiproj.fencemaster.base;
/** COMP30024 Artificial Intelligence
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

import java.util.ArrayList;

public class Board {
	
	private static Cell cells[][];
	public static ArrayList<Disk> diskWhite;
	public static ArrayList<Disk> diskBlack;
	public static ArrayList<Cell> emptyCells;
	public static int TotalCell;
	private static int Size;
	public static int N;
	
	/**
	 * Constructor of the board.
	 * @param N the width of the sides of the hexagonal board.
	 */
	public Board(int N) {
		Board.Size = 2*N-1;
		Board.N = N;
		Board.diskWhite = new ArrayList<Disk>();
		Board.diskBlack = new ArrayList<Disk>();
		Board.emptyCells = new ArrayList<Cell>();
		cells = new Cell[Size][Size]; // Example: N = 4, 7x7 array
		for(int i = 0; i < 2*N-1; i++) {
			for(int j = 0; j < 2*N-1; j++) {
				int usp = getUSP();		//upper starting point
				int lsp = getLSP(i);	//lower starting point
				int uep = getUEP(i);	//upper end point
				int lep = getLEP();	//lower end point
				cells[i][j] = new Cell(i, j);
				// Out of board checker
				if ((i<N && i<0) || j<usp) {
				}
				else if ((i<N && i<0) || j>uep) {
				}
				else if ((i>=N && i>Size-1) || j<lsp) {
				}
				else if ((i>=N && i>Size-1) || j>lep) {
				}
				else {
					emptyCells.add(cells[i][j]);
				}
			}
		}
		// Calculate the number of cells in the board for a given N
		TotalCell = 2*N-1;
		for (int i = N; i<(2*N-1); i++) {
			TotalCell += 2*i;
		}
	}
	
	/**
	 * Gets the cell of position x and y.
	 * @param x x position (row)
	 * @param y y position (column)
	 * @return The cell
	 */
	public static Cell getCell(int x, int y) {
		int usp = getUSP();		//upper starting point
		int lsp = getLSP(x);	//lower starting point
		int uep = getUEP(x);	//upper end point
		int lep = getLEP();	//lower end point
		// Out of board checker
		if ((x<N && x<0) || y<usp) {
			return null;
		}
		else if ((x<N && x<0) || y>uep) {
			return null;
		}
		else if ((x>=N && x>Size-1) || y<lsp) {
			return null;
		}
		else if ((x>=N && x>Size-1) || y>lep) {
			return null;
		}
		else {
			return cells[x][y];
		}
	}
	
	/**
	 * Gets a list of Disks that are currently placed on the board.
	 * @param type The type of the disks.
	 * @return list of disks
	 */
	public static ArrayList<Disk> getAllDisk(char type) {
		if(type == 'W') {
			return Board.diskWhite;
		} else {
			return Board.diskBlack;
		}
	}
	
	/**
	 * Gets the total number of cells
	 * @return total cell
	 */
	public int getTotalCell() {
		return TotalCell;
	}
	
	/**
	 * Gets the size of the board
	 * @return size
	 */
	public static int getSize() {
		return Size;
	}
	
	/**
	 * Gets the upper starting point (upper left boundary)
	 * @return USP
	 */
	public static int getUSP() {
		return 0;
	}
	
	/**
	 * Gets the upper ending point (upper right boundary)
	 * @return UEP
	 */
	public static int getUEP(int x) {
		return N+x-1;
	}
	
	/**
	 * Gets the lower starting point (lower left boundary)
	 * @return LSP
	 */
	public static int getLSP(int x) {
		return x-N+1;
	}
	
	/**
	 * Gets the lower ending point (lower right boundary)
	 * @return LEP
	 */
	public static int getLEP() {
		return 2*N-2;
	}
	
	/**
	 * Prints the current state of the board in a hexagon shape.
	 */
	public static void printBoard() {
		for(int i = 0; i < Board.getSize(); i++) {
			if (i < Board.N) {
				for (int s=0; s < Board.N-1-i ;s++) {
					System.out.print(" ");
				}
			} else if (i >= Board.N) {
				for (int s=0; s < Board.getLSP(i); s++) {
					System.out.print(" ");
				}
			}
			for(int j = 0; j < Board.getSize(); j++) {
				if (Board.getCell(i, j) == null) {
					continue;
					//System.out.print("x ");
				} 
				else if (Board.getCell(i, j).getStatus() == 0) {
					System.out.print("- ");
				}
				else {
					System.out.print(Board.getCell(i, j).getDisk().getType() + " ");
				}
			}
			System.out.println();
		}
		System.out.println();
	}
}
