package aiproj.fencemaster.base;
import java.util.Comparator;


public class fscoreComparator implements Comparator<Disk> {
	@Override
	public int compare(Disk a, Disk b) {
		return a.getFscore() < b.getFscore() ? -1 : a.getFscore() == b.getFscore() ? 0 : 1;
	}
}
