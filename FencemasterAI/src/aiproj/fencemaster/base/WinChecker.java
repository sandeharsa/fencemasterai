package aiproj.fencemaster.base;
/** COMP30024 Artificial Intelligence
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;

public class WinChecker {
	
	private static final int LOOPTRESH = 6;
	private static final int HEXAGON = 6;
	
	/**
	 * Checks whether the current state of the board is a winning state
	 * and print the according message.
	 * The checking is done in the function LoopChecker and TripodChecker.
	 */
	public static void TestWin() {
		boolean whiteT, whiteL, whiteB;
		boolean blackT, blackL, blackB;
		boolean finalstate;
		
		// Check which disk wins and by what method
		blackL = LoopChecker('B');
		whiteL = LoopChecker('W');
		blackT = TripodChecker('B');
		whiteT = TripodChecker('W');
		blackB = blackL && blackT;
		whiteB = whiteL && whiteT;
		
		// check if the board is in final state
		// which is the board is full of disks, no empty space
		finalstate = true;
		search:
		for (int i=0; i<Board.getSize(); i++) {
			for (int j=0; j<Board.getSize(); j++) {
				if (Board.getCell(i, j) == null) {
					continue;
				} else if (Board.getCell(i, j).getStatus() == 0) {
					finalstate = false;
					break search;
				}
			}
		}
		
		// Print out the result
		if (finalstate && !whiteL && !whiteT && !blackL && !blackT) {
			System.out.println("Draw");
			System.out.println("Nil");
		} else if(!finalstate && !whiteL && !whiteT && !blackL && !blackT) {
			System.out.println("None");
			System.out.println("Nil");
		} else if (blackB) {
			System.out.println("Black");
			System.out.println("Both");
		} else if (whiteB) {
			System.out.println("White");
			System.out.println("Both");
		} else if (blackL) {
			System.out.println("Black");
			System.out.println("Loop");
		} else if (whiteL) { 
			System.out.println("White");
			System.out.println("Loop");
		} else if (blackT) {
			System.out.println("Black");
			System.out.println("Tripod");
		} else if (whiteT) {
			System.out.println("White");
			System.out.println("Tripod");
		}
	}
	
	/**
	 * Finds a Loop on the board.
	 * This function uses the greedy best-frist search algorithm with straight line distance as its heuristic.
	 * @param node current disk of recursive iteration
	 * @param goal the goal disk
	 * @param prev previous disk, to avoid the path going back to the previous disk
	 * @param loop list of disks in a loop
	 * @return Whether there is a Loop or not
	 */
	private static boolean LoopCheckerRecurse(Disk node, Disk goal, Disk prev, ArrayList<Disk> loop) {
		loop.add(node);
		if (node == goal && loop.size() >= LOOPTRESH) {
			return true;
		}
		Collections.sort(node.getNeighbor(), new sldComparator(goal));
		for (Disk newnode : node.getNeighbor()) {
			if(loop.contains(newnode) || newnode == prev)
				continue;
			return LoopCheckerRecurse(newnode, goal, node, loop);
		}
		return false;
	}
	
	/**
	 * Finds a Loop on the board
	 * This function applies the LoopCheckerRecurse and
	 * uses Ray Casting method to check if the loop encircles at least
	 * one empty space or one opponent's disk
	 * @param type type of disk needed to be checked
	 * @return Whether there is a Loop or not
	 */
	public static boolean LoopChecker(char type) {
		ArrayList<Disk> loop = new ArrayList<Disk>();
		// Array List to store the minY and maxY each row
		int edgeY[][] = new int[Board.getSize()][2];
		int minX, maxX;
		
		boolean yesprint = false;
		
		// Iterate each disk to look for possible loops
		for (Disk goal: Board.getAllDisk(type)) {
			for (Disk node: goal.getNeighbor()) {
				loop.clear(); // Clear loop list
				node.setFscore(node.sld(goal));
				if(yesprint)
					System.out.println("Checking for loop ----");
				if(LoopCheckerRecurse(node, goal, goal, loop)) {
					minX = Board.getSize();		// Initialize minX
					maxX = 0;					// Initialize maxX
					for (int i = 0; i < Board.getSize(); i++) {
						edgeY[i][0] = Board.getSize();	// Initialize minY
						edgeY[i][1] = -1;				// Initialize maxX
					}
					for (Disk disk: loop) {
						// Always updates minX, maxX, minY, and maxY for each possible loops
						if (disk.getX() < minX) {
							minX = disk.getX();
						}
						if (disk.getX() > maxX) {
							maxX = disk.getX();
						}
						if (edgeY[disk.getX()][0] >= 0 && edgeY[disk.getX()][1] <= Board.getSize()) {
							if (disk.getY() < edgeY[disk.getX()][0]) {
								edgeY[disk.getX()][0] = disk.getY();
							}
							if (disk.getY() > edgeY[disk.getX()][1]) {
								edgeY[disk.getX()][1] = disk.getY();
							}
						}
						else {
							edgeY[disk.getX()][0] = disk.getY();
							edgeY[disk.getX()][1] = disk.getY();
						}
					}
					// RAY CASTING
					// Use the method Ray Casting to check if there is at least
					// one empty space or one opponent's disk inside the loop
					for (int x=minX+1; x<maxX; x++) {
						if(edgeY[x][0] == edgeY[x][1]) {
							continue;
						}
						if (Board.getCell(x, edgeY[x][0]+1).getStatus() == 0 ||
								Board.getCell(x, edgeY[x][0]+1).getDisk().getType() != type) {
							return true;
						}
					}
				}
			}
		}
		return false; // No loop found
	}
	
	/**
	 * Finds a Tripod on the board.
	 * This function uses the A* algorithm with straight line distance as its heuristic.
	 * @param goal the goal disk
	 * @param pathcost current cost of the path (each level increase the pathcost by 1)
	 * @param queue priority queue sorted by pathcost + sld
	 * @param tripod list of disks in a tripod
	 * @return Whether there's a Tripod or not
	 */
	private static boolean TripodCheckerRecurse(Disk goal, int pathcost, PriorityQueue<Disk> queue, ArrayList<Disk> tripod) {
		if (queue.peek() != null) {
			Disk node = queue.poll();
			node.setFscore(pathcost + node.sld(goal));
			tripod.add(node);
			if (node == goal) {
				return true;
			}
			
			for (Disk newnode : node.getNeighbor()) {
				if(tripod.contains(newnode) || queue.contains(newnode))    
					continue;
				newnode.setFscore(newnode.sld(goal) + pathcost);
				queue.add(newnode);
			}
			return TripodCheckerRecurse(goal, pathcost+1, queue, tripod);
		}
		return false;
	}
	
	/**
	 * Return an ArrayList of Disk that are on the sides of the game board.
	 * Side 1: top
	 * Side 2: top-left
	 * Side 3: top-right
	 * Side 4: bot-left
	 * Side 5: bot-right
	 * Side 6: bot
	 * @param allDisk all disk in the current Board
	 * @param type type of disk needed to be checked
	 * @return
	 */
	public static ArrayList<ArrayList<Disk>> sideDisk(ArrayList<Disk> allDisk, char type) {
		ArrayList<ArrayList<Disk>> side = new ArrayList<ArrayList<Disk>>(HEXAGON);
		for(int i = 0; i < HEXAGON; i++) {
			side.add(new ArrayList<Disk>());
		}
		int x;
		int y;
		
		for(int i = 0; i < allDisk.size(); i++) {
			if(allDisk.get(i).getType() != type) {
				continue;
			}
			x = allDisk.get(i).getX();
			y = allDisk.get(i).getY();
			if(x == 0 && y > Board.getUSP() && y < Board.getUEP(x)) {						// Top
				side.get(0).add(allDisk.get(i));
			} else if(x == Board.getLEP() && y > Board.getLSP(x) && y < Board.getLEP()) {	// Bot
				side.get(5).add(allDisk.get(i));
			} else if(x > 0 && x < Board.N-1 && y == Board.getUSP()) {						// Top-Left
				side.get(1).add(allDisk.get(i));
			} else if(x > 0 && x < Board.N-1 && y == Board.getUEP(x)) {						// Top-Right
				side.get(2).add(allDisk.get(i));
			} else if(x > Board.N-1 && x < 2*Board.N-2 && y == Board.getLSP(x)) {			// Bot-Left
				side.get(3).add(allDisk.get(i));
			} else if(x > Board.N-1 && x < 2*Board.N-2 && y == Board.getLEP()) {			// Bot-Right
				side.get(4).add(allDisk.get(i));
			}
		}
		
		return side;
	}
	
	/**
	 * This function applies the TripodCheckerRecurse to find a tripod
	 * It iterates through a list of side disk that exist on the board
	 * @return Whether there is a Tripod or not
	 */
	public static boolean TripodChecker(char type) {
		int counter;
		ArrayList<ArrayList<Disk>> side = sideDisk(Board.getAllDisk(type), type);
		ArrayList<Disk> tripod = new ArrayList<Disk>();
		PriorityQueue<Disk> queue =  new PriorityQueue<Disk>(1, new fscoreComparator());
		
		// Checks whether there are at least 3 side disks
		// There needs to be at least 3 side disks to make a loop
		int totalSideDisk = 0;
		for(ArrayList<Disk> s : side) {
			totalSideDisk += s.size();
		}
		if(totalSideDisk < 2) {
			return false;
		}
		
		// For each side disk, find a path to disks on the other sides.
		for(int i = 0; i < side.size();  i++) {
			// For-each node on a certain side
			for(Disk node : side.get(i)) {
				counter = 0;	// Reset counter for each node
				if(node.getNeighbor().size() == 0) {
					continue;
				}
				for(int j = 0; j < side.size(); j++) {
					if(i != j) {
						for(Disk goal : side.get(j)) { // for-each on the other sides
							tripod.clear(); // Clear tripod list
							queue.clear();
							node.setFscore(node.sld(goal));
							queue.add(node);
							if(TripodCheckerRecurse(goal, 0, queue, tripod)) {
								counter++;
							}
							if(counter >= 2) {	// Found the tripod
								return true;
							}
							break;
						}
					}
				}
			}
		}
		return false;	// No tripod found
	}
	
}
