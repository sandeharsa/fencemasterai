package aiproj.fencemaster.base;

import java.util.Comparator;

public class nodeComparator implements Comparator<Node> {
	@Override
	public int compare(Node a, Node b) {
		return a.value < b.value ? -1 : a.value == b.value ? 0 : 1;
	}
}