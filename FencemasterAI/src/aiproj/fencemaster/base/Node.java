package aiproj.fencemaster.base;

import java.util.ArrayList;
import java.util.List;

import aiproj.fencemaster.Move;

public class Node {
	Node parent;
	public Move move;
	int value;
	public List<Node> children;
	
	public Node(Move move, int value) {
		this.value = value;
		this.move = move;
		children = new ArrayList<Node>();
	}
	
	public void add_child(Node n) {
		children.add(n);
	}
	
	public int get_value() {
		return this.value;
	}
	
	public void update_value(int value) {
		this.value = value;
	}
	
	public void add_parent(Node n) {
		n.add_child(this);
		this.parent = n;
	}
}
