package aiproj.fencemaster.base;

import java.util.Collections;

import aiproj.fencemaster.*;


public class MinimaxTree {
	Node root;
	
	public MinimaxTree(Node n) {
		this.root = n;
	}
	
	public void change_root(Node n) {
		this.root = n;
	}
	
	public Node get_root() {
		return this.root;
	}
	
	/* Get the best move for the next move */
	public Move get_bestmove() {
		nodeComparator nc = new nodeComparator();
		Node bestnode = Collections.max(this.root.children, nc);
		return bestnode.move;
	}
}
