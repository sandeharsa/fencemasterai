package aiproj.fencemaster.base;
/** COMP30024 Artificial Intelligence
 *  Authors:
 *  1. Johan Albert (albertj - 567375)
 *  2. Sande Harsa (sharsa - 567642)
 */

import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Game {
	
	public int HEXAGON = 6;
	public static Board board;

	public static void main(String args[]) throws FileNotFoundException {
		// Getting input from stdin
		Scanner scanner = new Scanner(new InputStreamReader(System.in));
		int N = scanner.nextInt();
		scanner.nextLine();
		
		// Initialize Board
		board = new Board(N);
		
		// Read through the input file and place disks accordingly
		// Note: USP, UEP, LSP, LEP are boundary points for the hexagonal board
		String line[];		// Line from the input
		int row = 0;		// row
		while(scanner.hasNext() == true && row < 2*N-1) {
			line = scanner.nextLine().split(" ");
			if (row<N) {
				if (line.length != Board.getUEP(row)+1) {	
					System.out.println("Error input.");
					System.exit(1);
				}						
				for (int col=Board.getUSP(), i=0; col<=Board.getUEP(row) && i<line.length; col++, i++) {
					if(!line[i].contains("-")) {
						Board.getCell(row, col).placeDisk(row, col, line[i].charAt(0));
					}
				}
			}
			else if (row>=N) {
				if (line.length != Board.getLEP()-Board.getLSP(row)+1) {
					System.out.println("Error input.");
					System.exit(1);
				}
				for (int col=Board.getLSP(row), i=0; col<=Board.getLEP() && i<line.length; col++, i++) {
					if(!line[i].contains("-")) {
						Board.getCell(row, col).placeDisk(row, col, line[i].charAt(0));
					}
				}
			}
			
			row++;
		}
		
		if(row != 2*N-1) {
			System.out.println("Error input.");
			System.exit(1);
		}
		
		// Print the board
		//System.out.println("Current Board State");
		//System.out.println();
		//Board.printBoard();
		
		// Checking Winner
		WinChecker.TestWin();
	}
}
